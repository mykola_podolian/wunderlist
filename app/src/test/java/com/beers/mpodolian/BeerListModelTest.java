package com.beers.mpodolian;

import com.beers.mpodolian.dao.Beer;
import com.beers.mpodolian.dao.BeerDao;
import com.beers.mpodolian.networking.BeerAPI;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.TestSubscriber;

import java.util.Collections;
import java.util.List;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BeerListModelTest {

    private BeerListModel beerListModel;
    @Mock private BeerDao beerDao;
    @Mock private BeerAPI beerAPI;
    private Scheduler uiScheduler = Schedulers.trampoline();
    private Scheduler workerScheduler = Schedulers.trampoline();

    @Before
    public void setUp() throws Exception {
        beerListModel = new BeerListModel(beerDao, beerAPI, uiScheduler, workerScheduler);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testGetBeers_databaseQueried() throws Exception {
        //given
        List<Beer> beers = Collections.EMPTY_LIST;
        when(beerDao.getAll()).thenReturn(Flowable.just(beers));
        when(beerAPI.beersList(anyInt())).thenReturn(Single.just(Collections.EMPTY_LIST));
        TestSubscriber<List<Beer>> subscriber = new TestSubscriber();
        //when
        beerListModel.getBeers().subscribe(subscriber);
        //then
        verify(beerDao).getAll();
        subscriber.onNext(beers);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testGetBeers_databaseRefreshed() throws Exception {
        //given
        when(beerDao.getAll()).thenReturn(Flowable.just(Collections.EMPTY_LIST));
        when(beerAPI.beersList(anyInt())).thenReturn(Single.just(Collections.EMPTY_LIST));
        //when
        beerListModel.getBeers();
        //then
        verify(beerAPI).beersList(anyInt());
        verify(beerDao).insertOrReplace(Collections.EMPTY_LIST);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testSearch_databaseQueried() throws Exception {
        //given
        List<Beer> beers = Collections.EMPTY_LIST;
        String query = "ab";
        when(beerDao.search(anyString())).thenReturn(Flowable.just(beers));
        when(beerAPI.beersList(anyInt())).thenReturn(Single.just(Collections.EMPTY_LIST));
        TestSubscriber<List<Beer>> subscriber = new TestSubscriber();
        //when
        beerListModel.search(query).subscribe(subscriber);
        //then
        verify(beerDao).search("%" + query + "%");
        subscriber.onNext(beers);
    }

}