package com.beers.mpodolian;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import com.beers.mpodolian.dao.BeerDao;
import com.beers.mpodolian.networking.Beer;
import com.beers.mpodolian.networking.BeerAPI;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

import java.util.List;

public class BeerListModel extends ViewModel {

    private final static String LOG_TAG = "BeerListModel";
    private BeerAPI beerAPI;
    private BeerDao beerDao;
    private Scheduler uiScheduler;
    private Scheduler workerScheduler;
    private PublishSubject<Boolean> beersListUpdateCompletedChannel = PublishSubject.create();

    static BeerListModel of(FragmentActivity context) {
        Application application = (Application) context.getApplication();
        BeerListModel viewModel = ViewModelProviders.of(context).get(BeerListModel.class);
        viewModel.beerAPI = application.getBeerAPI();
        viewModel.beerDao = application.getBeerDataBase().beerDao();
        return viewModel;
    }

    public BeerListModel() {
        uiScheduler = AndroidSchedulers.mainThread();
        workerScheduler = Schedulers.io();
    }

    @VisibleForTesting
    BeerListModel(BeerDao beerDao,
        BeerAPI beerAPI,
        Scheduler uiScheduler,
        Scheduler workerScheduler)
    {
        this.beerDao = beerDao;
        this.beerAPI = beerAPI;
        this.uiScheduler = uiScheduler;
        this.workerScheduler = workerScheduler;
    }

    /**
     * Provides chanel with update complete result state. <code>true</code> if update succeed, <code>false</code>
     * otherwise.
     *
     * @return update complete success state channel
     */
    Observable<Boolean> getUpdateCompleted() {
        return beersListUpdateCompletedChannel;
    }

    /**
     * Provides channel with all cached entries. Invocation of this method also has side effect of refreshing
     * local list from the server.
     *
     * @return all entries chanel
     */
    Flowable<List<com.beers.mpodolian.dao.Beer>> getBeers() {
        updateDataBase();
        return beerDao.getAll()
            .observeOn(uiScheduler)
            .subscribeOn(workerScheduler);
    }

    /**
     * Provides channel with entries matching search query. Search performed on every field of
     * {@link com.beers.mpodolian.dao.Beer}
     *
     * @param query search query
     * @return all entries chanel
     */
    Flowable<List<com.beers.mpodolian.dao.Beer>> search(String query) {
        return beerDao.search("%" + query + "%")
            .observeOn(uiScheduler)
            .subscribeOn(workerScheduler);
    }

    private void updateDataBase() {
        beerAPI.beersList(50)
            .subscribeOn(workerScheduler)
            .observeOn(uiScheduler)
            .subscribe(this::processServerBeersList, this::processAPICallError);
    }

    private void processAPICallError(Throwable e) {
        Log.e(LOG_TAG, "Could not retrieve bears list from server", e);
        beersListUpdateCompletedChannel.onNext(false);
    }

    private void processServerBeersList(List<Beer> beers) {
        Observable.fromIterable(beers)
            .map(b ->
            {
                com.beers.mpodolian.dao.Beer daoBeer = new com.beers.mpodolian.dao.Beer();
                daoBeer.setId(b.getId());
                daoBeer.setName(b.getName());
                daoBeer.setImageUrl(b.getImageUrl());
                daoBeer.setTagLine(b.getTagLine());
                return daoBeer;
            })
            .toList()
            .subscribe(this::insertToDatabase);
    }

    private void insertToDatabase(List<com.beers.mpodolian.dao.Beer> beers) {
        Single.create(emitter -> {
            try {
                beerDao.insertOrReplace(beers);
                emitter.onSuccess(new Object());
            } catch (Exception e) {
                emitter.onError(e);
            }
        })
            .observeOn(uiScheduler)
            .subscribeOn(workerScheduler)
            .subscribe(o -> beersListUpdateCompletedChannel.onNext(true),
                e -> beersListUpdateCompletedChannel.onNext(false));
    }

}
