package com.beers.mpodolian.networking;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

import java.util.List;

public interface BeerAPI {

    @GET("beers")
    Single<List<Beer>> beersList(@Query("per_page") int beersPerPage);

}
