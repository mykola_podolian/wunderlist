package com.beers.mpodolian.networking;

import com.google.gson.annotations.SerializedName;

public class Beer {
    @SerializedName("id") private long id;
    @SerializedName("name") private String name;
    @SerializedName("image_url") private String imageUrl;
    @SerializedName("tagline") private String tagLine;

    public long getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getImageUrl() {
        return imageUrl;
    }
    public String getTagLine() {
        return tagLine;
    }
}
