package com.beers.mpodolian;

import android.arch.persistence.room.Room;

import com.beers.mpodolian.dao.BeerDatabase;
import com.beers.mpodolian.networking.BeerAPI;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.concurrent.TimeUnit;

public class Application extends android.app.Application {

    private BeerAPI beerAPI;
    private BeerDatabase beerDatabase;

    @Override
    public void onCreate() {
        super.onCreate();
        createBeerApi();
        createDatabase();
    }

    public BeerDatabase getBeerDataBase() {
        return beerDatabase;
    }

    public BeerAPI getBeerAPI() {
        return beerAPI;
    }

    private void createBeerApi() {
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .build();
        beerAPI = new Retrofit.Builder()
            .baseUrl("https://api.punkapi.com/v2/")
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(BeerAPI.class);
    }

    private void createDatabase() {
        beerDatabase = Room.databaseBuilder(getApplicationContext(),
            BeerDatabase.class, "beers").build();
    }
}
