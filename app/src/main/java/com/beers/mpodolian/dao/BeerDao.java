package com.beers.mpodolian.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import io.reactivex.Flowable;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

@Dao
public interface BeerDao {

    @Query("SELECT * FROM beer")
    Flowable<List<Beer>> getAll();

    @Query("SELECT * FROM beer WHERE name LIKE :query OR tag_line LIKE :query")
    Flowable<List<Beer>> search(String query);
    
    @Insert(onConflict = REPLACE)
    void insertOrReplace(List<Beer> beers);


}
