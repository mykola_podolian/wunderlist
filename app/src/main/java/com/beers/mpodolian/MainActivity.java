package com.beers.mpodolian;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.beers.mpodolian.dao.Beer;
import com.bumptech.glide.Glide;

import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Disposable currentBeersSubscription;
    private Disposable updateResultsSubscription;
    private BeerListModel beerListModel;
    private BeerAdapter beerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.beers.mpodolian.R.layout.activity_main);
        setSupportActionBar((Toolbar) findViewById(R.id.tool_bar));
        beerListModel = BeerListModel.of(this);
        beerAdapter = new BeerAdapter();
        beerAdapter.setItemClickListener(this::onListItemClick);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.beers_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(beerAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        subscribe(beerListModel.getBeers());
        updateResultsSubscription = beerListModel.getUpdateCompleted().subscribe(this::onUpdateCompleted);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                subscribe(beerListModel.search(s));
                return true;
            }
        });
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        disposeCurrentSubscription();
        updateResultsSubscription.dispose();
    }

    private void onUpdateCompleted(boolean successful) {
        String message = successful ? getString(R.string.update_succeed) : getString(R.string.update_failed);
        Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
    }

    private void onListItemClick(Beer beer) {
        String imageUrl = beer.getImageUrl();
        try {
            Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(imageUrl));
            startActivity(myIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, R.string.err_no_browser_installed, Toast.LENGTH_LONG).show();
        }
    }

    private void subscribe(Flowable<List<Beer>> channel) {
        disposeCurrentSubscription();
        currentBeersSubscription = channel.subscribe(beerAdapter::setItems);
    }

    private void disposeCurrentSubscription() {
        if (currentBeersSubscription != null && !currentBeersSubscription.isDisposed()) {
            currentBeersSubscription.dispose();
        }
    }

    private static class BeerAdapter extends RecyclerView.Adapter<BeerViewHolder> {

        public interface OnItemClickListener {
            void onItemClick(Beer beer);
        }

        private List<Beer> items = new ArrayList<>();
        private OnItemClickListener itemClickListener;

        @Override
        public BeerViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
            View itemView = layoutInflater.inflate(R.layout.raw_beer, viewGroup, false);
            itemView.setOnClickListener(this::onItemClick);
            return new BeerViewHolder(itemView);
        }
        @Override
        public void onBindViewHolder(BeerViewHolder viewHolder, int i) {
            Beer beer = items.get(i);
            Context context = viewHolder.itemView.getContext();
            Glide.with(context).load(beer.getImageUrl()).into(viewHolder.image);
            viewHolder.name.setText(beer.getName());
            viewHolder.tagLine.setText(beer.getTagLine());
            viewHolder.itemView.setTag(beer);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        void setItemClickListener(OnItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }

        void setItems(List<Beer> beers) {
            items.clear();
            items.addAll(beers);
            notifyDataSetChanged();
        }

        private void onItemClick(View itemView) {
            if (itemClickListener != null) {
                Beer beer = (Beer) itemView.getTag();
                itemClickListener.onItemClick(beer);
            }
        }

    }

    private static class BeerViewHolder extends RecyclerView.ViewHolder {

        private TextView name;
        private TextView tagLine;
        private ImageView image;

        private BeerViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
            tagLine = (TextView) itemView.findViewById(R.id.tag_line);
            image = (ImageView) itemView.findViewById(R.id.image);
        }
    }
}
